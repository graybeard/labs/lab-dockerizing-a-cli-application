# Lab > Docker + CLI

> Explore how to use docker to build a CLI application.


## Table of Contents

* [Why `oclif`](#why-**oclif**)
  * [Generating Commands](#generating-commands)
* [Docker](#using-docker-to-containerize-the-cli)
  * [Dockerfile](#dockerfile)
  * [Build](#docker-build)
  * [Run](#docker-running-the-container)
* [Resources](#resources)

---

## Why `oclif`

> For this lab we will be exploring using Docker to run a nodejs ClI application using [oclif](https://oclif.io/)

`oclif` is a good alternative to building a CLI application in NodeJS. The commands/topics can be organized to facilitate anything that you need.

`oclif` also has a great [plugin architecture](https://oclif.io/docs/plugins) to help organize shared features.

`oclif` has [hooks](https://oclif.io/docs/hooks) capacities that allow you to run code before something happens in the lifecycle events.

`oclif` allows you to [run commands programmatically](https://oclif.io/docs/running_programmatically)


### Generating Commands

We generate an oclif application called winger in the `examples/winger` folder.

```bash
npx oclif generate winger
```

Within the `examples/winger/` folder you can run this command:

```bash
bin/run hello friend --from tyler
# hello friend from tyler! (./src/commands/hello/index.ts)
```

This command will run the hello command, and pass the flag `--from` to the `CLI`.

> HINT: When you make changes to the cli, don't forget to rebuild the code.  Sometimes we forget to rebuild the cli and our commands and changes don't show up in their respective places.

## Using Docker to Containerize the CLI

When you are ready to use your CLI, and wish to publish it--building a container and tagging it with an appropriate tag and version will be important.

Using docker for CLI applications helps teams focus on using the right language/tool for the job.  As things like serverless, and functional programming make the environments more like having a ton of functions that do various things, docker is usually the smallest common denominator in most cloud systems.  On top of that, by having a knowledge of standing up infrastructure with a CLI will help with that journey of using Docker to abstract away the intrinsic details of the environment that the CLI requires to run

### Using NPM to run the scripts

Inside the `package.json` file (in the winger directory), there is a scripts section that has various scripts that can be run.  You will notice there are some at the bottom that are prefixed with `docker:*`, these are docker specific commands that help build and run the containers.  As an engineer, I like to use `npm` as a facilitator of my build scripting, and if you are using node--it is built into you environment.

#### Use the `scripts/test`

Running `npm test` will run the commands that are registered in the scripts scection of the `package.json`.

```packagejsonjson
"scripts": {
  "build": "shx rm -rf dist && tsc -b",
  "lint": "eslint . --ext .ts --config .eslintrc",
  "postpack": "shx rm -f oclif.manifest.json",
  "posttest": "npm run lint",
  "prepack": "npm run build && oclif manifest && oclif readme",
  "test": "mocha --forbid-only \"test/**/*.test.ts\"",
  "version": "oclif readme && git add README.md",
  "docker:build": "docker build -t winger .",
  "docker:run:hello": "docker run winger hello friend --from oclif"
}
```

To build the docker container use this command:

```bash
npm run docker:build
```

After you build the container, running the command below will run the container, and provides and example of how to run the containerized CLI application.

```bash
npm run docker:run:hello
```


### DockerFile

Create a `Dockerfile`, and confiture it for your necessities.

```docker
# Base image of the docker container
FROM node:latest
# Copy the contents of the repo into the /app folder inside the container
COPY . /app
# Update the current working directory to the /app folder
WORKDIR /app
# Add your CLI's installation steps here
RUN npm install && npm link
ENTRYPOINT ["/usr/local/bin/boilerplate"]
```

### Docker Build
Commands to build your

```bash
# Run the commands in our Dockerfile and create a container
# -t can be used to set name:tag of the built image
$ docker build -t your-cli-app .
Sending build context to Docker daemon   21.5MB
...
Successfully built 3fdbb4987594
Successfully tagged your-cli-app:latest
```

### Docker Running the Container
Now that your container is built, has a version, and a tagged name. You can run the container using some of the commands below.

```bash
# Run the latest `your-cli-app` container and pass it arguments like normal
$ docker run your-cli-app --version
1.0.0
$ docker run your-cli-app --help
Usage: boilerplate -d [/path/to/directory/]
```

## Resources

* [Oclif](https://oclif.io/)
* [Dockerizing CLI application](https://www.howtogeek.com/devops/how-to-use-docker-to-package-cli-applications/)
* [Make your CLI portable](https://levelup.gitconnected.com/make-your-cli-portable-with-docker-c97038fdc601)
* [Creating a CLI toolkit](https://medium.com/oracledevs/creating-an-oracle-cloud-infrastructure-cli-toolkit-docker-image-35be0ca71aa)
* [Dockerized CLI](https://github.com/benzaita/dockerized-cli)

### packages

* [Superstruct Docs](https://docs.superstructjs.org/) and [Repository](https://github.com/ianstormtaylor/superstruct) -- Useful for validation of types.
*
